#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include "gpio_blink.h"
#include "acelerometro.h"

extern char buf[6];
extern unsigned char data[];

int sock, n; // sock: fd for the socket

void sigintHandler(int sig_num);

int main(int argc, char **argv){



	struct sockaddr_in server, client; //addresses
	struct hostent *hp; // information of the host
	socklen_t length=sizeof(server); // size of server address

	const int S_DATA = 14;
	const int N_DATA = 11;
	const char HOLA_SERVER[] = "HELLO SERVER";
	const char HOLA_RASPI[] = "HELLO RASPI";
	const char ALIVE[]="STILL ALIVE";
	const char ACK[] = "ACK";
	const char END[] = "TERMINATE";
	char buffer[256];
	int iteracion = 1;
	char bufferDatos[(S_DATA * (N_DATA-1))];
	sensor colorsensor = config();
    sensor aceler = MMA8451_Init();


	if(argc != 3){
		printf("Usage: server port\n");
		exit(1);
	}


	signal(SIGINT, sigintHandler);


    memset((char *)&server,0,sizeof(server));
	server.sin_family = AF_INET;
	hp=gethostbyname(argv[1]); // argumento 1= IP address


	if(hp == 0){
		printf("ERROR: unknown host\n");
		exit(0);
	}else{
		printf("IP address assigned \n");
	}

	/*for(int i=0; i<140; i++){
		bufferDatos[i]=0;
		}*/

	bcopy((char *)hp->h_addr, (char *)&server.sin_addr, hp->h_length); //assings ip adress to host

	server.sin_port = htons(atoi(argv[2])); // assigns the port passsed as parameter 2

	//create socket(server/client)
	sock = socket(AF_INET,SOCK_DGRAM,0);
	if(sock < 0){
		printf("ERROR: socket error\n");
		exit(0);
	}else {
		printf("Socket created \n");
	}



	printf("Sending.. \n");
	////Comunicacion establecimiento de conexion/////
	n = sendto(sock, HOLA_SERVER, sizeof(HOLA_SERVER), 0, (struct sockaddr *)&server, length); //envía HELLO_SERVER

	// Checks that Server answered in less than 10s to "Hello server"
		for(int i = 0; i<10; i++){

			n = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *)&client, &length);

			if(strcmp(buffer, HOLA_RASPI) == 0){
			   printf("Message received: OK\n");
			   i=10;
			}else{
		      printf("Waiting for answer...\n");
			   }

			  sleep(1);
			}

			if(n < 0){
				 printf("NO ANSWER \n");
				 close(sock);
				 exit(0);
			}




	printf("Data Mode\n");


	while (1){ // Modo datos



		 if(iteracion == N_DATA){ // Si han pasado 10 sleep(1), enviamos datos

			  n = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *)&client, &length);

			   if(strcmp(buffer, END) == 0){
					   printf("\n Message received: Terminate\n");
					      close(sock);
					      fflush(stdout);
					      exit(1);

			   }else if(strcmp(buffer, ALIVE) == 0){

				   printf("\n Message received: Still Alive\n");

				 iteracion = 1;

				 printf("Sending Data\n");
				 n = sendto(sock, bufferDatos, sizeof(bufferDatos), 0, (struct sockaddr *)&server, length);

				 if(n>0){
					 printf(" - Data sent\n");
				 }

				 n = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *)&client, &length);

				 if(strcmp(buffer, ACK) == 0){
					 printf(" - ACK Recieved\n\n\n\n\n");


				 }else if(strcmp(buffer, END) == 0){

					 // FIN DEL PROGRAMA
					 printf("Message received: Terminate\n");
					 close(sock);
					 fflush(stdout);
					 exit(0);
				 }
			   }
			 }else{ // Si han pasado menos de 10 segundos  hacemos mediciones


				 printf("Measure %i: \n", iteracion);

				 MMA8451_Read_All(&aceler);


				 for(int i=0; i<6; i++){
					 bufferDatos[((iteracion-1)*S_DATA)+i] = buf[i];//acel

				  }

				 reading_data(&colorsensor);

				   for(int i=0; i<8; i++){
					 bufferDatos[((iteracion-1)*S_DATA)+6+i] = data[i]; //color

				    }

					printf("[");
					for(int i=0; i<140; i++){
						printf("%x,", bufferDatos[i]);
					 }
				     printf("]\n\n");
					iteracion++;
			     }


			 sleep(1);

	}
	close(sock);
	return 0;
}

void sigintHandler(int sig_num){
    // Reset handler to catch SIGINT next time.
    printf("\n SigINT captured... Closing Server\n");
    close(sock);
    fflush(stdout);
    exit(1);
}
