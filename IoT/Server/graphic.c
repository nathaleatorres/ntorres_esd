#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include "graphic.h"

#define NUM_PUNTOS 10
#define NUM_COMANDOS 4


extern int dataR[];
extern int dataG[];
extern int dataB[];

extern double dataX[];
extern double dataY[];
extern double dataZ[];




void Rdatafile(){
    // X
    int valoresX[NUM_PUNTOS] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    register int i=0;


    FILE * archivoPuntos = fopen("DataRed.txt", "w");

    for (i=0;i<NUM_PUNTOS;i++){
      fprintf(archivoPuntos, "%3d  %3d  %3d  %3d \n", valoresX[i], dataR[i],dataG[i],dataB[i]);
 }

    /*lista de comandos para ejecutar y configurar la visualización que tendrán
     * los puntos en la gráfica con gnuplot*/
    char * configGnuplot[] = { "set title \"RGB data\"",
                                "set ylabel \"----Color level--->\"",
                                "set xlabel \"----Measurement--->\"",
                                "plot \"DataRed.txt\" using 1:2 with lines title \"RED\" lt rgb \"red\", \"DataRed.txt\" using 1:3 with lines title \"GREEN\" lt rgb \"green\", \"DataRed.txt\" using 1:4 with lines title \"BLUE\" lt rgb \"blue\" "
                               };


    FILE * ventanaGnuplot = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one

    for (i=0;i<NUM_COMANDOS;i++){
      fprintf(ventanaGnuplot, "%s \n", configGnuplot[i]);
    }

     fclose(archivoPuntos);

}

void Xdatafile(){
    // X
    int valoresX[NUM_PUNTOS] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    register int i=0;


    FILE * archivoPuntos = fopen("DataAcel.txt", "w");

    for (i=0;i<NUM_PUNTOS;i++){
      fprintf(archivoPuntos, "%3d  %.2f  %.2f  %.2f \n", valoresX[i], dataX[i],dataY[i],dataZ[i]);
 }

    /*lista de comandos para ejecutar y configurar la visualización que tendrán
     * los puntos en la gráfica con gnuplot*/
    char * configGnuplot[] = {  " set title \"Acelerometer data\"",
                                "set ylabel \"----m/s²--->\"",
                                "set xlabel \"----Measurement--->\"",
                                "plot \"DataAcel.txt\" using 1:2 with lines title \"X\" , \"DataAcel.txt\" using 1:3 with lines title \"Y\", \"DataAcel.txt\" using 1:4 with lines title \"Z\" "
                              };


    FILE * ventanaGnuplot = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one

    for (i=0;i<NUM_COMANDOS;i++){
      fprintf(ventanaGnuplot, "%s \n", configGnuplot[i]);
    }

     fclose(archivoPuntos);
}


/*void Gdatafile(){
    // X
    int valoresX[NUM_PUNTOS] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    register int i=0;


    FILE * archivoPuntos = fopen("DataGreen.txt", "w");

    for (i=0;i<NUM_PUNTOS;i++){
      fprintf(archivoPuntos, "%d  %d \n", valoresX[i], dataG[i]);
 }


    char * configGnuplot[] = {"set title \"Green data\"",
                                "set ylabel \"----Green level--->\"",
                                "set xlabel \"----Measurement--->\"",
                                "plot \"DataGreen.txt\" using 1:2 with lines"
                               };


    FILE * ventanaGnuplot = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one

    for (i=0;i<NUM_COMANDOS;i++){
      fprintf(ventanaGnuplot, "%s \n", configGnuplot[i]);
    }

} */



/*void Bdatafile(){
    // X
    int valoresX[NUM_PUNTOS] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    register int i=0;


    FILE * archivoPuntos = fopen("DataGreen.txt", "w");

    for (i=0;i<NUM_PUNTOS;i++){
      fprintf(archivoPuntos, "%d  %d \n", valoresX[i], dataG[i]);
 }


    char * configGnuplot[] = {"set title \"Green data\"",
                                "set ylabel \"----Green level--->\"",
                                "set xlabel \"----Measurement--->\"",
                                "plot \"DataGreen.txt\" using 1:2 with lines"
                               };


    FILE * ventanaGnuplot = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one

    for (i=0;i<NUM_COMANDOS;i++){
      fprintf(ventanaGnuplot, "%s \n", configGnuplot[i]);
    }

} */



/*void Ydatafile(){
    // X
    int valoresX[NUM_PUNTOS] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    register int i=0;


    FILE * archivoPuntos = fopen("DataGreen.txt", "w");

    for (i=0;i<NUM_PUNTOS;i++){
      fprintf(archivoPuntos, "%d  %d \n", valoresX[i], dataG[i]);
 }


    char * configGnuplot[] = {"set title \"Green data\"",
                                "set ylabel \"----Green level--->\"",
                                "set xlabel \"----Measurement--->\"",
                                "plot \"DataGreen.txt\" using 1:2 with lines"
                               };


    FILE * ventanaGnuplot = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one

    for (i=0;i<NUM_COMANDOS;i++){
      fprintf(ventanaGnuplot, "%s \n", configGnuplot[i]);
    }

} */



/*void Zdatafile(){
    // X
    int valoresX[NUM_PUNTOS] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    register int i=0;


    FILE * archivoPuntos = fopen("DataGreen.txt", "w");

    for (i=0;i<NUM_PUNTOS;i++){
      fprintf(archivoPuntos, "%d  %d \n", valoresX[i], dataG[i]);
 }


    char * configGnuplot[] = {"set title \"Green data\"",
                                "set ylabel \"----Green level--->\"",
                                "set xlabel \"----Measurement--->\"",
                                "plot \"DataGreen.txt\" using 1:2 with lines"
                               };


    FILE * ventanaGnuplot = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one

    for (i=0;i<NUM_COMANDOS;i++){
      fprintf(ventanaGnuplot, "%s \n", configGnuplot[i]);
    }

} */

