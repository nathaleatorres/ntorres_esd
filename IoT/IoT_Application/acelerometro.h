

#ifndef ACELEROMETRO_H_
#define ACELEROMETRO_H_

#include <stdio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <unistd.h>
#include <stdint.h>
#include "i2c_bus.h"

#define I2C_DEVICE              "/dev/i2c-1"
#define I2C_ADDR_MMA8451         0x1D
#define READING_REGISTER_DATA    0x00
#define WHOIAM                   0x0D




int fd;
char portName[20];
char reg[2];


sensor MMA8451_Init();
void MMA8451_Read_All(sensor *handle);

#endif /* ACELEROMETRO_H_ */
