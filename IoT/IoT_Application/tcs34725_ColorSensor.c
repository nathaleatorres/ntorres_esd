
#include "tcs34725_ColorSensor.h"
#include "i2c_bus.h"

unsigned char data[8];

uint16_t R = 0;
uint16_t G = 0;
uint16_t B = 0;

static uint16_t catBytes(uint16_t highB, uint16_t lowB){

	return ((uint16_t)highB << 8) | (uint16_t)lowB;

}


sensor config (){
	sensor handle;

    handle.fd = -1; //Start as uninitialized
    handle.address = ADDR;
	char i2cFile[11];
    int device = 1;

    printf("Initalising TCS34725 sensor at address %#02x\n", handle.address);

    sprintf(i2cFile, "/dev/i2c-%d", device);

    handle.fd = open(i2cFile, O_RDWR); //Obtain file descriptor for RW
    if (handle.fd < 0 ){
      printf("Error: file descriptor opening\n");
    }
    int v_ioctl = ioctl(handle.fd, I2C_SLAVE, ADDR); //Configure the file for I2C communications with slave
    if(v_ioctl < 0)
      printf("Error: ioctl couldn´t wake colorsensor\n");
    // Select enable register(0x80)
    // Power ON, RGBC enable, wait time disable(0x03)
    write_byte(&handle, ENA_REG, 0x03);
    // Select ALS time register(0x81)
	// Atime = 700 ms(0x00)
    write_byte( &handle, ATIME_REG, 0x00);
    // Select Wait Time register(0x83)
    // WTIME : 2.4ms(0xFF)
    write_byte( &handle, WTIME_REG, 0xFF);
    // Select control register(0x8F)
    // AGAIN = 1x(0x00)
    write_byte( &handle, CONTROL_REG, 0x02);
    //ID REGISTERs
    if(read_byte(&handle, ID_REG) == 0x44)
    	printf("Colorsensor TCS34725 detected\n");
    else{
    	printf("Colorsensor TCS34725 NOT DETECTED. Connect TCS34725...\n");
        exit (1);
    }

    return handle;
}

void reading_data(sensor *handle){
	// Read 8 bytes of data from register(0x94)
	// cData lsb, cData msb, red lsb, red msb, green lsb, green msb, blue lsb, blue msb

		size_t n = 8;
		read_multibyte(handle, CDATAL, data, n);

		printf("\n Data color: [");
		 for(int i=0; i<8; i++){
		   printf("%x,", data[i]);
		}
		printf("]\n");


			R= catBytes(data[3],data[2])/256;
			G= catBytes(data[5],data[4])/256;
			B= catBytes(data[7],data[6])/256;


			// Output data to screen
			printf("RGB: %d,%d,%d \n", R,G,B);


}

