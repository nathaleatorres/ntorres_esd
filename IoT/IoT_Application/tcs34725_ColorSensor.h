#ifndef TCS34725_COLORSENSOR_H_
#define TCS34725_COLORSENSOR_H_

#define ADDR  0x29 //7-bit Slave address
#define ENA_REG 0x80
#define ATIME_REG 0x81
#define WTIME_REG 0x83
#define CONTROL_REG 0X8F
#define ID_REG 0X92
#define CDATAL 0xB4 //0x94 non repetitive
#define CDATAH 0x95
#define RDATAL 0x96
#define RDATAH 0x97
#define GDATAL 0x98
#define GDATAH 0x99
#define BDATAL 0x9A
#define BDATAH 0x9B




typedef struct tcs34725_{
	int fd;
	unsigned char address;
	char data[8];
}
sensor;

/**
 * Configuration of sensor TCS34725
 * @return colorsensor
 */
sensor config ();

/**
 * Transform the data received from the sensor through I2C
 * @param handle
 */
void reading_data(sensor *handle);


#endif /* TCS34725_COLORSENSOR_H_ */
