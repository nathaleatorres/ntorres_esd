

#ifndef I2C_BUS_H_
#define I2C_BUS_H_

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <linux/i2c.h>
#include <string.h>
#include <sys/stat.h>

#include "tcs34725_ColorSensor.h"

//DECLARATION
char read_byte(sensor *handle, int reg);
void write_byte(sensor *handle, int reg, char data);
void read_multibyte(sensor *handle, int reg, unsigned char* output, size_t len);

#endif /* I2C_BUS_H_ */
