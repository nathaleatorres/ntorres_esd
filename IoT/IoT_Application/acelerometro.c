#include "acelerometro.h"

static int16_t catBytes(int8_t highB, int8_t lowB){

	return ((((int16_t)highB << 8) | ((int16_t)lowB))>>2);

     }

char buf[6];

sensor MMA8451_Init(){

	sensor handle;
	handle.address = I2C_ADDR_MMA8451;
	handle.fd = -1;
	char i2cFile[11];
	int device = 1;

	printf("Initalising ACELEROMETER at address %#02x\n", handle.address);

	sprintf(i2cFile, "/dev/i2c-%d", device);

	handle.fd= open(i2cFile, O_RDWR);
	if(fd < 0) {
			printf("Error: file descriptor opening\n");
      }
	if(ioctl(handle.fd, I2C_SLAVE, I2C_ADDR_MMA8451) < 0){
			printf("Error: ioctl couldnt wake acelerometer\n");
	}
     // setting range
     write_byte(&handle, 0x0E, 0x01);
     // setting active mode
     write_byte(&handle, 0x2A, 0x01);

     if(read_byte(&handle, WHOIAM) == 0x1A)
         	printf("ACELEROMETER MMA8451Q detected\n");
         else{
         	printf("ACELEROMETER MMA8451Q NOT DETECTED. Connect MMA8451Q...\n");
             exit (1);
         }

     return handle;

}


void MMA8451_Read_All(sensor *handle){
// To return the data read


        float axlex=0;
        float axley=0;
        float axlez=0;

        //datos que se envian al servidor rawdata
		buf[0] = read_byte(handle, 0x01);
		buf[1] = read_byte(handle, 0x02);
		buf[2] = read_byte(handle, 0x03);
		buf[3] = read_byte(handle, 0x04);
		buf[4] = read_byte(handle, 0x05);
		buf[5] = read_byte(handle, 0x06);

		printf("\n Data acelereometro: [");
		 for(int i=0; i<6; i++){
		   printf("%x,", buf[i]);
		}
		printf("]\n");

    int16_t Pos_X = catBytes(buf[0],buf[1]);
         axlex=(float)Pos_X/2048;

   
	int16_t Pos_Y = catBytes(buf[2],buf[3]);
        axley=(float)Pos_Y/2048;;


  	int16_t Pos_Z = catBytes(buf[4],buf[5]);
         axlez=(float)Pos_Z/2048;


         printf("XYZ: %.2f,%.2f,%.2f \n ",axlex,axley,axlez);




}
