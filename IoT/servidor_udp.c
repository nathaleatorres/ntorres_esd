#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include "graphic.h"


const int S_DATA = 14;


double axlex=0;
double axley=0;
double axlez=0;

int dataR[10];
int dataG[10];
int dataB[10];
double dataX[10];
double dataY[10];
double dataZ[10];


uint16_t R = 0;
uint16_t G = 0;
uint16_t B = 0;


void data_process(int received[]);

void alive ();

void sigintHandler(int sig_num);

static int16_t catBytesA(int8_t highB, int8_t lowB){
	return ((int16_t)highB << 8) | (int16_t)lowB;
}

static uint16_t catBytesC(uint8_t highB, uint8_t lowB){
	return ((uint16_t)highB << 8) | (uint16_t)lowB;
}


int IS_ACTIVE = 1;

int  fd_socket = -1;

struct sockaddr_in server, client; // adresses
socklen_t address_size;




int main(int argc, char **argv){


	int  rec = -1;
	int port = atoi(argv[1]);//setting port. converting to int

	const char HOLA_SERVER[] = "HELLO SERVER";
	const char HOLA_RASPI[] = "HELLO RASPI";
	const char ACK[] = "ACK";
    const char END[] = "TERMINATE";
	char cadena [sizeof(END)];






	// Signal Handler
	signal(SIGINT, sigintHandler);


    // Arguments
	if(argc != 2){
		printf("USAGE: %s <port>\n", argv[0]);
		exit(0);
	}


	//Create a new socket of type SOCK_DGRAM (udp socket)
	//in domain AF_INET (i'm giving the ip address for the computer to understand it), using
	//protocol 0 (chosen automatically)
	fd_socket = socket(AF_INET, SOCK_DGRAM, 0);


	if(fd_socket < 0){
			printf("ERROR: Opening socket\n");
			exit(0);
		}

	//Initializing structure
	memset(&server, '\0', sizeof(server));
	memset(&client, '\0', sizeof(client));

	//Setting up server
	server.sin_family = AF_INET;//type of address
	server.sin_port = htons(port);//setting the port. high so it is not used
	server.sin_addr.s_addr = INADDR_ANY;// ADDRESS
	printf(" Socket created successfully\n");

	int connection_status = bind(fd_socket, (struct sockaddr *) &server, sizeof(server));//assign address to server

	if(connection_status < 0){
		printf(" ERROR: Couldnt bind \n");
		exit(0);
	}else{
	printf(" Bind successfull \n");
	}

    address_size=sizeof(client);

    char buffer[140];
    	    //data received WAIT FOR MSG
    		rec =  recvfrom(fd_socket, buffer, sizeof(buffer), 0, (struct sockaddr*)&client, &address_size);


    		if(rec >= 0){
    			printf("Lenght of Data Received -> %i\n", rec);

    		}

    			if(strcmp(buffer, HOLA_SERVER) == 0){
    				printf("Menssage recieved: 'HELLO SERVER'\n");
    				sendto(fd_socket, HOLA_RASPI, sizeof(HOLA_RASPI), 0, (struct sockaddr *)&client, address_size);
    				printf("Menssage sent: 'HELLO RASPI'\n");

    			}else{
    				printf("WRONG MESSAGE\n");
    				sendto(fd_socket, "WRONG MESSAGE",20, 0, (struct sockaddr *)&client, address_size);
    				close(fd_socket);
    				return 0;
    			}



	while(IS_ACTIVE){

		 alive();

		 rec =  recvfrom(fd_socket, buffer, sizeof(buffer), 0, (struct sockaddr*)&client, &address_size);

			printf("\n DATA RECEIVED:\n");

			int received[rec];

			printf("[");
			for(int i=0; i<rec; i++){
				received[i] = (int)buffer[i] & (0x000000ff);
				// Descomentar para ver array que envia RASPI
				printf("%x,", received[i]);
			}
			printf("]\n\n");


			data_process(received);

			Rdatafile();
			//Gdatafile();
			//Bdatafile();
			Xdatafile();
			//Ydatafile();
			//Zdatafile();

			printf("\n ACK o TERMINATE:\n");

			scanf("%s",cadena);

			if(strcmp(cadena, ACK) == 0){
			 // Se envia ACK
		      sendto(fd_socket, ACK, sizeof(ACK), 0, (struct sockaddr *)&client, address_size);
			  printf("------------------\n");
			}else if (strcmp(cadena, END) == 0) {
				  // Se envia TERMINATE
			      sendto(fd_socket, END, sizeof(END), 0, (struct sockaddr *)&client, address_size);
			      printf("Message Sent: Terminate\n");
				  printf("------------------\n");

			}

	}
	return 0;
}

/*Processing the data*/
void data_process(int received[]){

	 int16_t Pos_X;
	 int16_t Pos_Y;
	 int16_t Pos_Z;


	for(int i =0; i<10;i++){

	  Pos_X = catBytesA(received[0+i*S_DATA],received[1+i*S_DATA]);
	          axlex=(float)Pos_X/8192;



	  Pos_Y = catBytesA(received[2+i*S_DATA],received[3+i*S_DATA]);
	      axley=(float)Pos_Y/8192;


	  Pos_Z = catBytesA(received[4+i*S_DATA],received[5+i*S_DATA]);
	        axlez=(float)Pos_Z /8192;



	      R= catBytesC(received[9+i*S_DATA],received[8+i*S_DATA])/256;
	      G= catBytesC(received[11+i*S_DATA],received[10+i*S_DATA])/256;
	      B= catBytesC(received[13+i*S_DATA],received[12+i*S_DATA])/256;


	      dataR[i]=R;
		  dataG[i]=G;
		  dataB[i]=B;
		  dataX[i]=axlex;
		  dataY[i]=axley;
		  dataZ[i]=axlez;

	  printf("\n RGB %d : (%d,%d,%d)",(i+1),R,G,B);
	  printf("\n XYZ %d: (%.2f,%.2f,%.2f) \n",i,axlex,axley,axlez);

	 }
 }


/*Periodic Function to send message */

void alive(){

	const char ALIVE[]="STILL ALIVE";

	for(int i=0;i<10;i++){
		sleep(1);
	}
	printf("Message Sent: Still Alive\n");
	sendto(fd_socket, ALIVE, sizeof(ALIVE), 0, (struct sockaddr *)&client, address_size);

}

/* Signal Handler for SIGINT */
void sigintHandler(int sig_num){
    // Reset handler to catch SIGINT next time.
    //sendto(fd_socket, "END", 3, 0, (struct sockaddr *)&client, address_size);
    //printf("Message Sent: Terminate\n");
    printf("\n SigINT captured... Closing Server\n");

    IS_ACTIVE = 0;

    fflush(stdout);
    close(fd_socket);
    exit(1);
}
